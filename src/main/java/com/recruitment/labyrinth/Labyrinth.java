package com.recruitment.labyrinth;

import java.util.List;

import com.recruitment.labyrinth.room.RoomLinkFactory;
import com.recruitment.labyrinth.room.RoomsLink;
import com.recruitment.labyrinth.walker.Walker;

public class Labyrinth {

	private List<RoomsLink> roomsLinks;
	private Walker walker;
	private SensorsReader sensorsReader;
	
	public Labyrinth(final String ... labyrinthTokens) {
		this.roomsLinks = RoomLinkFactory.createRoomLinksFrom(labyrinthTokens);
		this.walker = new Walker(roomsLinks);
		this.sensorsReader = new SensorsReader();
	}

	public void popIn(String roomName) {
		walker.startFrom(roomName);
	}

	public void walkTo(String roomName) {
		walker.walkTo(roomName);
	}

	public void closeLastDoor() {
		walker.closeLastGate();
	}

	public String readSensors() {
		return sensorsReader.read(walker);
	}

}
