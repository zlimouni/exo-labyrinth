package com.recruitment.labyrinth.walker;

import com.recruitment.labyrinth.room.Room;

public class Move {

	private final Room sourceRoom;
	private final Room destinationRoom;
	
	public Move(Room source, Room destination) {
		this.sourceRoom = source;
		this.destinationRoom = destination;
	}
	public Room getSourceRoom() {
		return sourceRoom;
	}

	public Room getDestinationRoom() {
		return destinationRoom;
	}
	
}
