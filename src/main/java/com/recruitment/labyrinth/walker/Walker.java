package com.recruitment.labyrinth.walker;

import java.util.List;
import java.util.Optional;

import com.recruitment.labyrinth.exception.ClosedDoorException;
import com.recruitment.labyrinth.exception.DoorAlreadyClosedException;
import com.recruitment.labyrinth.exception.IllegalMoveException;
import com.recruitment.labyrinth.room.Room;
import com.recruitment.labyrinth.room.RoomsLink;

public class Walker{

	private Room source;
	private Room destination;
	private List<RoomsLink> roomsLinks;
	private Move move;
	
	public Walker(List<RoomsLink> roomsLinks) {
		this.roomsLinks = roomsLinks;
	}

	public void startFrom(String roomName) {
		this.source = new Room(roomName);
	}
	
	public void walkTo(String roomName) {
		this.destination = new Room(roomName);
		this.move = new Move(source, destination);
		
		if(!isMoveInRoomsroomsLinks()) 
			throw new IllegalMoveException();
		
		checkIfGateClosed();
		
		this.source = destination;
	}

	public void checkIfGateClosed() {
		RoomsLink roomsLink = roomsLinks.stream().filter(r->r.equals(move)).findFirst().get();
		if(roomsLink.getGate().isClosed()) {
			throw new ClosedDoorException();
		}
	}

	public boolean isMoveInRoomsroomsLinks() {
		Optional<RoomsLink> optionalRoomLink = roomsLinks.stream().filter(r->r.equals(move)).findFirst();
		if(optionalRoomLink.isPresent()) {
			return true;
		}
		
		return false;
	}

	public void closeLastGate() {
		RoomsLink roomsLink = roomsLinks.stream().filter(r->r.equals(move)).findFirst().get();
		
		if(isLastGateClosed(roomsLink)) {
			throw new DoorAlreadyClosedException();
		}else {
			roomsLink.getGate().closeGate();
		}
		
	}

	public boolean isLastGateClosed(RoomsLink roomsLink) {
		return roomsLink.getGate().isClosed();
	}

	public Move getMove() {
		return move;
	}

	public List<RoomsLink> getRoomsLinks() {
		return roomsLinks;
	}
	

}