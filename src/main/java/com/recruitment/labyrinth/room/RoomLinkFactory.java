package com.recruitment.labyrinth.room;

import java.util.ArrayList;
import java.util.List;

import com.recruitment.labyrinth.gate.GateFactory;
import com.recruitment.labyrinth.gate.GateType;

public class RoomLinkFactory {

	public static List<RoomsLink> createRoomLinksFrom(String[] labyrinthTokens) {
		List<RoomsLink> roomsLinks = new ArrayList<>();
		
		for (String labyrinthToken : labyrinthTokens) {
			Character sourceRoomLetter = labyrinthToken.charAt(0);
			Character destinationRoomLetter = labyrinthToken.charAt(2);
			Character gateLetter = labyrinthToken.charAt(1);
			
			Room source = new Room(sourceRoomLetter.toString());
			Room destination = new Room(destinationRoomLetter.toString());

			RoomsLink roomsLink = new RoomsLink(source, destination);
			
			if(gateLetter.toString().equals(GateType.NORMAL.getSymbol())) {
				roomsLink.setGate(GateFactory.createGate(GateType.NORMAL));
			} else {
				roomsLink.setGate(GateFactory.createGate(GateType.SENSOR));
			}
			
			roomsLinks.add(roomsLink);
		}
		
		
		return roomsLinks;
	}

}
