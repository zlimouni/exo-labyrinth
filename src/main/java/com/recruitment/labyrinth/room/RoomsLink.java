package com.recruitment.labyrinth.room;

import com.recruitment.labyrinth.gate.Gate;
import com.recruitment.labyrinth.walker.Move;

public class RoomsLink {

	private final Room source;
	private final Room destination;
	private Gate gate;

	public RoomsLink(Room source, Room destination) {
		this.source = source;
		this.destination = destination;
	}

	public void setGate(Gate gate) {
		this.gate = gate;
	}
	public Room getSource() {
		return source;
	}
	public Room getDestination() {
		return destination;
	}
	public Gate getGate() {
		return gate;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if(this == obj)  return true;
		if(obj instanceof Move) {
			return (source.getName().equals(((Move) obj).getSourceRoom().getName()) && destination.getName().equals(((Move) obj).getDestinationRoom().getName())) ||
					(source.getName().equals(((Move) obj).getDestinationRoom().getName()) && destination.getName().equals(((Move) obj).getSourceRoom().getName()));

		}else {
			return false;
		}
	}
}
