package com.recruitment.labyrinth;

import com.recruitment.labyrinth.gate.GateType;
import com.recruitment.labyrinth.room.RoomsLink;
import com.recruitment.labyrinth.walker.Move;
import com.recruitment.labyrinth.walker.Walker;

public class SensorsReader {

	private static final String SEPARATOR = ";";
	private StringBuilder representation;
	
	public SensorsReader() {
		this.representation = new StringBuilder();
	}

	public String read(Walker walker) {
		Move move =  walker.getMove();
		RoomsLink roomsLink = walker.getRoomsLinks().stream().filter(r->r.equals(move)).findFirst().get();
		
		if(!isNormalGate(roomsLink)) {
			representation.append(move.getSourceRoom().getName())
			  .append(move.getDestinationRoom().getName())
			  .append(SEPARATOR);
		}
				
		return representation.substring(0,representation.length()-1);
	}

	public boolean isNormalGate(RoomsLink roomsLink) {
		return GateType.NORMAL.getSymbol().equals(roomsLink.getGate().getSymbol());
	}

}
