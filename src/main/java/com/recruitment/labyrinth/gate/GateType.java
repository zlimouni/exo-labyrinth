package com.recruitment.labyrinth.gate;

public enum GateType {

	NORMAL("|"), SENSOR("$");
	
	private String symbol;
	
	private GateType(String symbol) {
		this.symbol = symbol;
	}
	
	public String getSymbol() {
		return this.symbol;
	}
}
