package com.recruitment.labyrinth.gate;

class SensorGate extends Gate {

	@Override
	public String getSymbol() {
		return GateType.SENSOR.getSymbol();
	}

}
