package com.recruitment.labyrinth.gate;

public abstract class Gate {

	private boolean isClosed;

	public boolean isClosed() {
		return isClosed ? true : false;
	}
	public void closeGate() {
		isClosed = true;
	}
	
	public abstract String getSymbol();
}
