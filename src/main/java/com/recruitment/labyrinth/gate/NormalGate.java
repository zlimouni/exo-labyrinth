package com.recruitment.labyrinth.gate;

class NormalGate extends Gate {

	@Override
	public String getSymbol() {
		return GateType.NORMAL.getSymbol();
	}
	
}
