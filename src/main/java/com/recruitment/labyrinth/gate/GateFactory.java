package com.recruitment.labyrinth.gate;

public class GateFactory {

	public static Gate createGate(GateType type) {
		Gate gate = null;
		
		switch (type) {
			case NORMAL: 	gate = new NormalGate(); break;
			case SENSOR: 	gate = new SensorGate(); break;
		}
		
		return gate;
	}

}
